let num = 2
getCube = num**3
console.log(`The cube of ${num} is ${getCube}`)


const address = ["258", "Washington Ave.", "NW", "California", 90011]
const [houseNumber, street, state, country, zipCode] = address
console.log(`I live at ${houseNumber} ${street} ${state} ${country} ${zipCode}`)

const animal = {
	animalType: "Crocodile",
	weight: 1075,
	measurement: "20 ft 3"
}

const {animalType, weight, measurement} = animal
console.log (`Lolong was a saltwater ${animalType}. he weighed at ${1075} Kgs with a measuremant of ${measurement}`)

const numbers = [1, 2, 3, 4, 5]
numbers.forEach((number) => {
	console.log(number);
})

class Dog {
	constructor (name, age, breed){
	this.name = name
	this.age = age
	this.breed = breed
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5
myDog.breed =  "Miniature Dachshund"

console.log(myDog)

